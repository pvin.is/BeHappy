/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Button,
  StyleSheet,
  Text,
  View
} from 'react-native'

import Permissions from 'react-native-permissions'
import PushNotification from 'react-native-push-notification'

export default class BeHappy extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notificationPermissionState: false,
    }
  }

  checkPermissionState = async () => {
    const result = await Permissions.check('notification')
    this.setState({notificationPermissionState: result})
  }

  askPermission = async () => {
    const result = await Permissions.request('notification')
    this.setState({notificationPermissionState: result})
  }

  componentDidMount() {
    this.checkPermissionState()

    PushNotification.configure({
      onRegister: (token) => {
         console.log( 'TOKEN:', token)
      },
      onNotification: (notification) => {
          console.log( 'NOTIFICATION:', notification );
      },
      requestPermissions: false,
  });

  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text>state: {this.state.notificationPermissionState}</Text>
        <Button 
        title='Ask for permissions'
        onPress={() => {
          this.askPermission()
        }}
        />
        <Button
        title='Send test'
        onPress={() => {
          PushNotification.localNotification({
            title: 'Test notif',
            message: 'Test body',
          })
        }}
        />
        <Text style={styles.instructions}>
          To get started, edit index.ios.js
        </Text>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('BeHappy', () => BeHappy);
